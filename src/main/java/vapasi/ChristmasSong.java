package vapasi;

public class ChristmasSong {

    public static String buildChristmasSong() {
        StringBuilder strChristmasSong = new StringBuilder();
        for (int i = 1; i <= 12; i++) {
            strChristmasSong.append("On the " + getOrdinalForTheDay(i)
                                    + " day of Christmas my true love gave to me:"
                                    + getSongForTheDay(i) + ".\n");
        }
        return strChristmasSong.toString();
    }

    private static String getSongForTheDay(int day){
        String strSong="";
        switch (day) {
            case 1: strSong="a Partridge in a Pear Tree";break;
            case 2: strSong="two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 3: strSong="three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 4: strSong="four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 5: strSong="five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 6: strSong="six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 7: strSong="seven Swans-a-Swimming, six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 8: strSong="eight Maids-a-Milking, seven Swans-a-Swimming, six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 9: strSong="nine Ladies Dancing, eight Maids-a-Milking, seven Swans-a-Swimming, six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 10: strSong="ten Lords-a-Leaping, nine Ladies Dancing, eight Maids-a-Milking, seven Swans-a-Swimming, six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 11: strSong="eleven Pipers Piping, ten Lords-a-Leaping, nine Ladies Dancing, eight Maids-a-Milking, seven Swans-a-Swimming, six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
            case 12: strSong="twelve Drummers Drumming, eleven Pipers Piping, ten Lords-a-Leaping, nine Ladies Dancing, eight Maids-a-Milking, seven Swans-a-Swimming, six Geese-a-Laying, five Gold Rings, four Calling Birds, three French Hens, two Turtle Doves, and a Partridge in a Pear Tree";break;
        }
        return strSong;
    }

    private static String getOrdinalForTheDay(int day){
        String strOrdinal="";
        switch (day) {
            case 1: strOrdinal="first";break;
            case 2: strOrdinal="second";break;
            case 3: strOrdinal="third";break;
            case 4: strOrdinal="fourth";break;
            case 5: strOrdinal="fifth";break;
            case 6: strOrdinal="sixth";break;
            case 7: strOrdinal="seventh";break;
            case 8:  strOrdinal="eighth";break;
            case 9:  strOrdinal="ninth";break;
            case 10: strOrdinal="tenth";break;
            case 11: strOrdinal="eleventh";break;
            case 12: strOrdinal="twelfth";break;
        }
        return strOrdinal;
    }
}
