package vapasi;

import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ChristmasSongTest {

    @Test
    public void shouldNotReturnNullForChristmasSong() {
        String strSong = ChristmasSong.buildChristmasSong();
        assertThat((strSong == null),is(false));
    }

    @Test
    public void shouldNotReturnEmptyForChristmasSong() {
        String strSong = ChristmasSong.buildChristmasSong();
        assertThat((strSong == ""),is(false));
    }
}